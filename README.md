# Yahoo Weather

 A simple application using mvp pattern with retrofit and rxjava to get weather forecasts based on a given input (city) using yahoo 
 weather API
 
 # App Features
 
 1. MVP pattern
 2. edit text to enter search string, button to display results
 3. libraries used: Retrofit, RxJava, GSON
 4. app handles error cases like: empty input, network issues, invalid input
 5. handles orientation change
 
 # Improvements
 
 1. caching 
 2. UI ex: landscape layout (beyond the scope of the project)
 3. better implementation of design patterns (mvvm)
 
 # Issues
 
 1. Had difficulty in getting the right query for Yahoo weather API, no proper documentation

# How to run

1. Checkout master branch of the repository
2. Open project in android studio
2. Build & run project


