package scribd.com.weatherapp.network;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import scribd.com.weatherapp.model.Weather;

/**
 * interface for getting weather forecast from yahoo API
 */

public interface WeatherService {

    @GET("/v1/public/yql")
    Observable<Weather> getWeatherForecast(
            @Query("q") String query,
            @Query("format") String format);
}
