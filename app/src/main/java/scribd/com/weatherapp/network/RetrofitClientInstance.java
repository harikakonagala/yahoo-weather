package scribd.com.weatherapp.network;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {

    private final static String BASE_URL = "https://query.yahooapis.com/";
    private WeatherService weatherService;

    public RetrofitClientInstance() {
        this(BASE_URL);
    }

    /**
     * Builds Retrofit and WeatherService objects for calling the Yahoo weather API and parsing with GSON
     * @param url
     */
    public RetrofitClientInstance(String url) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create()) //converter factory for serialization and deserialization of objects
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) //Adapter factory for supporting service method return types
                .build();

        weatherService = retrofit.create(WeatherService.class);

    }

    public WeatherService getWeatherService() {
        return weatherService;
    }
}
