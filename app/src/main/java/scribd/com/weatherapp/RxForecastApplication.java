package scribd.com.weatherapp;

import android.app.Application;

import scribd.com.weatherapp.network.RetrofitClientInstance;

public class RxForecastApplication extends Application {

    private RetrofitClientInstance retrofitClientInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        retrofitClientInstance = new RetrofitClientInstance();
    }

    public RetrofitClientInstance getRetrofitClientInstance() {
        return retrofitClientInstance;
    }
}
