package scribd.com.weatherapp.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import scribd.com.weatherapp.R;
import scribd.com.weatherapp.model.Forecast;

/**
 * adapter class for displaying the forecast results
 */
public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ViewHolder> {

    private Context context;
    private List<Forecast> forecastList;

    public ForecastAdapter(Context context, List<Forecast> forecastList) {
        this.context = context;
        this.forecastList = forecastList;
    }

    //update the recyclerview
    public void setForecastList(List<Forecast> forecastList) {
        this.forecastList = forecastList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.forecast_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        Forecast forecast = forecastList.get(position);
        if(forecast != null) {
            viewHolder.date.setText(forecast.getDate());
            switch (forecast.getDay()) {
                case "Sun":
                    viewHolder.day.setText("Sunday");
                    break;
                case "Mon":
                    viewHolder.day.setText("Monday");
                    break;
                case "Tue":
                    viewHolder.day.setText("Tuesday");
                    break;
                case "Wed":
                    viewHolder.day.setText("Wednesday");
                    break;
                case "Thu":
                    viewHolder.day.setText("Thursday");
                    break;
                case "Fri":
                    viewHolder.day.setText("Friday");
                    break;
                case "Sat":
                    viewHolder.day.setText("Saturday");
                    break;
            }
            viewHolder.high.setText(forecast.getHigh() + " F");
            viewHolder.low.setText(forecast.getLow() + " F");
            viewHolder.currWeather.setText(forecast.getText());
        }
    }

    @Override
    public int getItemCount() {
        if(forecastList != null) {
            return forecastList.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView date, day, high, low, currWeather;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            date = itemView.findViewById(R.id.tv_date);
            day = itemView.findViewById(R.id.tv_day);
            high = itemView.findViewById(R.id.tv_high);
            low = itemView.findViewById(R.id.tv_low);
            currWeather = itemView.findViewById(R.id.tv_state);
        }
    }
}
