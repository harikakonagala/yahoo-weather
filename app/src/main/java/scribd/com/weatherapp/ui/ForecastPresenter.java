package scribd.com.weatherapp.ui;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import scribd.com.weatherapp.model.Weather;
import scribd.com.weatherapp.network.RetrofitClientInstance;

public class ForecastPresenter implements ForecastInteractor.Presenter {

    private ForecastInteractor.View view;
    private RetrofitClientInstance retrofitClientInstance;
    private CompositeDisposable disposable;

    public ForecastPresenter(ForecastInteractor.View view, RetrofitClientInstance retrofitClientInstance) {
        this.view = view;
        this.retrofitClientInstance = retrofitClientInstance;
        disposable = new CompositeDisposable();
    }

    @Override
    public void getWeatherForecast(String inputSearchString) {

        view.showProgressDialog();

        char unit = 'f';

        String YQL = String.format("select * from weather.forecast where woeid in (select woeid from geo.places(1) where " +
                "text=\"%s\") and u='" + unit + "'", inputSearchString);

        //check internet connection, otherwise show error message
        if(view.checkInternet()) {
            disposable.add(retrofitClientInstance.getWeatherService()
            .getWeatherForecast(
                    YQL,
                    "json"
            )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::handleResults, this::handleError));

        } else {
            view.hideProgressBar();
            view.invalidError();
        }
    }

    private void handleResults(Weather weather) {
        view.hideProgressBar();
        view.showResults(weather);
    }

    private void handleError(Throwable throwable) {
        view.hideProgressBar();
        view.showError(throwable);
    }

    @Override
    public void unSubscribe() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.clear();
        }
    }
}
