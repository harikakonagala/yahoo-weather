package scribd.com.weatherapp.ui.util;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * helper class for checking if the app is connected to internet
 */

public class CheckInternet {

    public static boolean isNetwork(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
