package scribd.com.weatherapp.ui;

import scribd.com.weatherapp.model.Weather;

public interface ForecastInteractor {

    interface Presenter {
        /**
         * method to return the parsed json results from yahoo weather API
         * @param inputSearchString
         */
        void getWeatherForecast(String inputSearchString);

        /**
         * method for unsubscribing the RxJava subscriptions
         */
        void unSubscribe();
    }

    interface View {

        /**
         * UI for loading state before getting the results
         */
        void showProgressDialog();

        /**
         * UI after fetching results
         */
        void hideProgressBar();

        /**
         * handles network errors
         */
        void invalidError();

        /**
         * checks internet connection
         * @return
         */
        boolean checkInternet();

        /**
         * handle error states for API call
         * @param throwable
         */
        void showError(Throwable throwable);

        /**
         * updates UI with the parsed results
         * @param weather
         */
        void showResults(Weather weather);
    }
}
