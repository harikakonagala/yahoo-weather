package scribd.com.weatherapp.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import scribd.com.weatherapp.R;
import scribd.com.weatherapp.RxForecastApplication;
import scribd.com.weatherapp.model.Channel;
import scribd.com.weatherapp.model.Forecast;
import scribd.com.weatherapp.model.Weather;
import scribd.com.weatherapp.network.RetrofitClientInstance;
import scribd.com.weatherapp.ui.util.CheckInternet;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        ForecastInteractor.View {

    // todo handle cache

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String ORIENTATION_FLAG = "ORIENTATION_FLAG";
    private boolean rxCallInProgress = false;


    EditText searchCity;
    Button getForecastButton;

    LinearLayout emptyContainer;
    TextView errorMessage;

    TextView city, currentWeatherState, currentHighTemp;
    RelativeLayout resultsLayout;
    View divider;
    RecyclerView displayWeatherList;
    ForecastAdapter forecastAdapter;
    private List<Forecast> forecastList;

    private String inputSearchString;

    private ProgressDialog pDialog;

    private RetrofitClientInstance retrofitClientInstance;
    private ForecastInteractor.Presenter forecastPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        retrofitClientInstance = ((RxForecastApplication) getApplication()).getRetrofitClientInstance();
        forecastPresenter = new ForecastPresenter(this, retrofitClientInstance);
        
        initViews();

        //get saved state during orientation change and update UI
        if(savedInstanceState != null) {
            rxCallInProgress = savedInstanceState.getBoolean(ORIENTATION_FLAG);
        }
    }

    private void initViews() {

        forecastList = new ArrayList<>();

        searchCity = findViewById(R.id.et_search);
        getForecastButton = findViewById(R.id.button_get_forecast);

        emptyContainer = findViewById(R.id.layout_error_message_container);
        errorMessage = findViewById(R.id.tv_errorMessage);

        city = findViewById(R.id.tv_city);
        currentWeatherState = findViewById(R.id.tv_current_state);
        currentHighTemp = findViewById(R.id.tv_high_temp);

        resultsLayout = findViewById(R.id.layout_results);
        divider = findViewById(R.id.view_divider);
        displayWeatherList = findViewById(R.id.rv_forecast);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false);
        displayWeatherList.setLayoutManager(mLayoutManager);
        displayWeatherList.setHasFixedSize(true);
        displayWeatherList.setItemAnimator(new DefaultItemAnimator());

        forecastAdapter = new ForecastAdapter(this, forecastList);
        displayWeatherList.setAdapter(forecastAdapter);

        //set default message for first time
        emptyContainer.setVisibility(View.VISIBLE);
        errorMessage.setText("No search results available");
        resultsLayout.setVisibility(View.GONE);
        divider.setVisibility(View.GONE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        //register listener
        getForecastButton.setOnClickListener(this);

        //get input from edit text field
        inputSearchString = searchCity.getText().toString();
        if(rxCallInProgress) {
            forecastPresenter.getWeatherForecast(inputSearchString);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //unregister listener
        getForecastButton.setOnClickListener(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //dispose subscriptions
        forecastPresenter.unSubscribe();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if(id == R.id.button_get_forecast) {
            rxCallInProgress = true;
            //get input from edit text field
            inputSearchString = searchCity.getText().toString();
            forecastPresenter.getWeatherForecast(inputSearchString);
        }
    }

    @Override
    public void showProgressDialog() {
        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage(getString(R.string.loading_message));
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        emptyContainer.setVisibility(View.GONE);
        resultsLayout.setVisibility(View.VISIBLE);
        divider.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        pDialog.dismiss();
    }

    @Override
    public void invalidError() {
        emptyContainer.setVisibility(View.VISIBLE);
        resultsLayout.setVisibility(View.GONE);
        divider.setVisibility(View.GONE);
        errorMessage.setText("Please check your Internet connection");
    }

    @Override
    public boolean checkInternet() {
        return CheckInternet.isNetwork(this);
    }

    @Override
    public void showResults(Weather weather) {
        if(weather != null) {
            Channel channel = weather.
                    getQuery().
                    getResults().
                    getChannel();

            city.setText(channel.getLocation().getCity());
            Log.i(TAG, channel.getLocation().getCity());

            currentWeatherState.setText(channel.getItem().getCondition().getText());
            Log.i(TAG, channel.getItem().getCondition().getText());

            String temperatureUnit = channel.getUnits().getTemperature();
            currentHighTemp.setText(channel.getItem().getCondition().getTemp() + " " + temperatureUnit);
            Log.i(TAG, channel.getItem().getCondition().getTemp());

            forecastList = channel.getItem().getForecast();
            Log.i(TAG, forecastList.size() + " \n" + forecastList.toString() + "\n");

            if(forecastList != null) {
                //update recyclerview
                forecastAdapter.setForecastList(forecastList);
                Log.i(TAG, forecastList.size() + " \n" + forecastList.toString() + "\n");
            }
        }
    }

    @Override
    public void showError(Throwable throwable) {
        String error = throwable.getMessage();
        emptyContainer.setVisibility(View.VISIBLE);
        resultsLayout.setVisibility(View.GONE);
        divider.setVisibility(View.GONE);
        errorMessage.setText(error);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //save state to reload UI during orientation changes
        outState.putBoolean(ORIENTATION_FLAG, rxCallInProgress);
    }
}
